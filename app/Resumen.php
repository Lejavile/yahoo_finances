<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resumen extends Model
{
    protected $table = "resumenes";

    protected $fillable = [
        'nombre',
        'tiker',
        'cierreAnterior',
        'abrir',
        'oferta',
        'demanda',
        'rangoDiario',
        'rango52Semanas',
        'volumen',
        'mediaVolumen',
        'tmtm',
        'ttm',
        'fechaBeneficios',
        'prevision'
    ];
}
