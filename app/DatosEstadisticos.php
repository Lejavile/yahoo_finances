<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DatosEstadisticos extends Model
{
    protected $table = "datos_estadisticos";

    protected $fillable = [
    	'tiker',
    	'titulo',
    	'valor1',
    	 	
    ];
}
