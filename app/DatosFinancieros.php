<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DatosFinancieros extends Model
{
    protected $table = "datos_financieros";

    protected $fillable = [
    	'tiker',
    	'titulo',
    	'valor1',
    	'valor2',
    	'valor3',
    	'valor4'  	
    ];
}
