<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //'App\Console\Commands\SendMail',
        'App\Console\Commands\SaveResumen',
        'App\Console\Commands\SaveHistory',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //$schedule->command('send:mail')->hourly();
        //$schedule->command('send:mail')->everyMinute();
        $schedule->command('save:resumen')->everyMinute();
        $schedule->command('save:history')->everyMinute();
        //->everyTenMinutes();
        //->everyMinute();
        //->dailyAt('03:00');
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
