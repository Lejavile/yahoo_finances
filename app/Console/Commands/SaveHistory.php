<?php

namespace App\Console\Commands;

use Illuminate\Http\Request;
use Illuminate\Console\Command;
use Scheb\YahooFinanceApi\ApiClient;
use Scheb\YahooFinanceApi\ApiClientFactory;
use GuzzleHttp\Client;
use App\Company;
use App\DatosHistoricos;

class SaveHistory extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'save:history';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Guardar historial diario';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $companies = Company::all();
        foreach ($companies as  $value) {
            $client = ApiClientFactory::createApiClient();

                //consultamos ultima fecha guardada en historicos
                $ult_historico=DatosHistoricos::where('tiker',$value->tiker)->orderby('date','DESC')->take(1)->first();
                //si trae ultima fecha le simamos un dia para que arranque de ese dia hasta ayer (yesterday) sino que rranque desde el inico de historicos del api.
                if (!empty($ult_historico)) {
                    $ultima_fecha=date("Y-m-d",strtotime($ult_historico->date."+ 1 days")); 
                    $historicalData = $client->getHistoricalData($value->tiker, ApiClient::INTERVAL_1_DAY, new \DateTime($ultima_fecha), new \DateTime("tomorrow"));
                }else{
                    $historicalData = $client->getHistoricalData($value->tiker, ApiClient::INTERVAL_1_DAY, new \DateTime("-13760 days"), new \DateTime("tomorrow"));
                }
                
                $jsonString1 = json_encode($historicalData);
                $arrayFormatHistory = json_decode($jsonString1, true);

                $n=2;
                $resta55 = count($arrayFormatHistory) - 55;
                $resta200 = count($arrayFormatHistory) - 200;
                $suma55 = 0;
                $suma200 = 0;

                
                for ($i=0; $i < count($arrayFormatHistory) ; $i++) { 
                    $arrayFormatHistory[$i]['date']['date'] = substr($arrayFormatHistory[$i]['date']['date'], 0, 10);
                    //promedio pms 55
                    if($resta55==$i){
                        $suma55=0;
                        for ($j=$resta55; $j > $resta55-55; $j--) { 
                            $suma55 = $suma55 + $arrayFormatHistory[$j]['close'];
                        }
                        $promedio55 = $suma55/55; 

                        $arrayFormatHistory[$i]['pms55'] = $promedio55;
                        $resta55=$resta55+1;
                        
                    }else{  
                        $arrayFormatHistory[$i]['pms55'] = 0;
                    }  

                    //promedio pms 200
                    if($resta200==$i){
                        $suma200=0;
                        for ($j=$resta200; $j > $resta200-200; $j--) { 
                            $suma200 = $suma200 + $arrayFormatHistory[$j]['close'];
                        }
                        $promedio200 = $suma200/200; 

                        $arrayFormatHistory[$i]['pms200'] = $promedio200;
                        $resta200=$resta200+1;
                        
                    }else{  
                        $arrayFormatHistory[$i]['pms200'] = 0;
                    } 
                    
                }




                //insertamos
                
                foreach($arrayFormatHistory as $item){


                    DatosHistoricos::create([
                        'tiker'=>$value->tiker,
                        'date'=>$item['date']['date'],
                        'open'=>$item['open'],
                        'high'=>$item['high'],
                        'low'=>$item['low'],
                        'close'=>$item['close'],
                        'pms55'=>$item['pms55'],
                        'pms200'=>$item['pms200'],
                        'adjClose'=>$item['adjClose'],
                        'volume'=>$item['volume']
                    ]);
               
                }


            $historicalData = $client->getHistoricalData($value->tiker, ApiClient::INTERVAL_1_DAY, new \DateTime("-13760 days"), new \DateTime("tomorrow"));


            $jsonString1 = json_encode($historicalData);
            $arrayFormatHistory = json_decode($jsonString1, true);

            foreach ($arrayFormatHistory as $item) {
                DatosHistoricos::create([
                    'tiker'=>$value->tiker,
                    'date'=>$item['date']['date'],
                    'open'=>$item['open'],
                    'high'=>$item['high'],
                    'low'=>$item['low'],
                    'close'=>$item['close'],
                    'adjClose'=>$item['adjClose'],
                    'volume'=>$item['volume']
                ]);

            }

        }
    }
}