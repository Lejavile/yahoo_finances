<?php

namespace App\Console\Commands;

use Illuminate\Http\Request;
use Illuminate\Console\Command;
use Scheb\YahooFinanceApi\ApiClient;
use Scheb\YahooFinanceApi\ApiClientFactory;
// Use GuzzleHttp for web scraping
use GuzzleHttp\Client;
use App\Company;
use App\Resumen;

class SaveResumen extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'save:resumen';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Guardar resume diario';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $companies = Company::all();
        foreach ($companies as  $value) {
            $client = ApiClientFactory::createApiClient();
            $quote = $client->getQuote($value->tiker);

            $jsonString = json_encode($quote);
            $arrayFormatQuote = json_decode($jsonString, true);

            $existe=resumen::where('tiker', $value->tiker)->where('fresumen', date('Y-m-d'))->first();
            if (empty($existe)) {
                
                Resumen::create([
                    'nombre' => $arrayFormatQuote['longName'],
                    'tiker' => $arrayFormatQuote['symbol'],
                    'cierreAnterior' => $arrayFormatQuote['regularMarketPreviousClose'],
                    'abrir' => $arrayFormatQuote['regularMarketOpen'],
                    'oferta' => $arrayFormatQuote['bid']." x ".$arrayFormatQuote['bidSize']*100,
                    'demanda' => $arrayFormatQuote['ask']." x ".$arrayFormatQuote['askSize']*100,

                    'rangoDiario' => $arrayFormatQuote['regularMarketDayLow'],
                    'rangoDiario_minimo' => $arrayFormatQuote['regularMarketDayHigh'],
                    'rango52Semanas' => $arrayFormatQuote['fiftyTwoWeekLow'],
                    'rango52Semanas_minimo' => $arrayFormatQuote['fiftyTwoWeekHigh'],
                    'volumen' => $arrayFormatQuote['regularMarketVolume'],
                    'mediaVolumen' => $arrayFormatQuote['averageDailyVolume3Month'],
                    'tmtm' => $arrayFormatQuote['trailingPE'],
                    'ttm' => $arrayFormatQuote['epsTrailingTwelveMonths'],
                    'fechaBeneficios' => $arrayFormatQuote['earningsTimestamp']['date'],
                    'prevision' => $arrayFormatQuote['trailingAnnualDividendRate']." (".($arrayFormatQuote['trailingAnnualDividendYield']*100)." %)",
                    'fresumen'=> date('Y-m-d')
                ]);

            }
        }
    }
}