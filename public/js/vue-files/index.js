var indexApp = new Vue({
	el: "#index-app",
	data: {
		allCompanies:[],
		showWrap: true,
	},
	created: function(){
		
	},
	methods: {
		getAllCompanies: function(search){
			indexApp.allCompanies = [];

			$.ajax({
				url: "ajax/get/all-companies/"+search,
				type:'GET',
				dataType: 'JSON',
				beforeSend: function(){
					indexApp.showWrap = false;
				},
				success: function(data){
					if(data.length>0){
						indexApp.showWrap = true;
						indexApp.allCompanies = data;
					}else{
						indexApp.showWrap = true;
					}
					

				}
			})
		},

		
	}
});