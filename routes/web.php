<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// ---- rutas oficiales ------ //
Route::get('/', 'ScrapingController@index');

Route::get('/empresa/{symbol}/resumen', 'CompanyController@resumen');
Route::get('/ajax/save/resumen/{symbol}', 'CompanyController@saveResumen');
Route::get('/ajax/save/history/{symbol}', 'CompanyController@saveHistory');
//-----------------------&/

//Route::get('/generar/excel/{tiker}', 'ScrapingController@generar');
Route::get('/generar/excel/historicos/{tiker}', 'ExcelGenerateController@generarHistoricos');

Route::get('/o', 'ScrapingController@o');

Route::get('/company/{tiker}', 'ScrapingController@getDataCompany');
Route::get('/company/{tiker}/financieros', 'ScrapingController@getDataFinancial');
Route::get('/company/{tiker}/estadisticos', 'ScrapingController@getDataEstadisticos');

Route::post('ajax/send/data', 'ScrapingController@receivedData');

// Ajax routes
Route::get('/ajax/get/all-companies/{searchWord}', 'ScrapingController@getAllCompanies');

//Route Get all Companies
Route::get('/companies','CompanyController@getFirstCompanies');
//rutas financieros
Route::get('/GetFinancieros','ScrapingController@indexFinancial');//vista
Route::get('/GetAllCompany','ScrapingController@getAllCompany');//compañias
Route::get('/GetDataFinancieros','ScrapingController@getDataFinancial');//data financieros
Route::post('/SaveDataFinancieros','ScrapingController@saveDataFinancial');//saveData financieros

//rutas estadisticos
Route::get('/GetEstaditicos','ScrapingController@indexEstadistico');//vista
Route::get('/GetDataEstadisticos','ScrapingController@getDataEstadisticos');//data financieros
Route::post('/SaveDataEstadisticos','ScrapingController@saveDataEstadistico');//saveData financieros




Route::get('/companies/{cantidad}', 'CompanyController@getRestCompanies');


//rutas ajax provisionales
Route::post('ajax/save/companies', 'CompanyController@saveCompanies');

