@extends('base')

@section('title')
Companies
@endsection

@section('container')
	
	<div class="html" style="display:none;color: red;"></div>
	<div class="row">
		<div class="col-md-12 text-center">	
			DATOS ESTADISTICOS
		</div>
		<div class="card col-md-6">
            <div class="card-body text-center">
                <h5 class="card-title">Companies o Tiker: Cargados</h5>
                <div class="d-flex no-block">
                	<div class="ml-auto">
                        <div id="prediction">Total</div>
                    </div>
                    <div class="align-self-end no-shrink">
                        <h2 class="m-b-0 " id="idtotalCargados">----<img class="image hidden" style="width: 40px;" src="{{ asset('img/ajax_loading.gif') }}"></h2> 
                    </div>
                </div>
            </div>
        </div>
        <div class="card col-md-6">
            <div class="card-body text-center">
                <h5 class="card-title">Companies o Tiker: Pendiente por Cargar</h5>
                <div class="d-flex no-block">
                	<div class="ml-auto">
                        <div id="prediction">Total</div>
                    </div>
                    <div class="align-self-end no-shrink">
                        <h2 class="m-b-0 " id="idtotalCarga">----<img class="image hidden" style="width: 40px;" src="{{ asset('img/ajax_loading.gif') }}"></h2> 
                    </div>
                </div>
            </div>
        </div>
        <div class="card col-md-6">
            <div class="card-body text-center">
                <h5 class="card-title">Ultima Carga</h5>
                <div class="d-flex no-block">
                	<div class="ml-auto">
                        <div id="">Fecha</div>
                    </div>
                    <div class="align-self-end no-shrink">
                        <h2 class="m-b-0" id="idFecha">----<img class="image hidden" style="width: 40px;" src="{{ asset('img/ajax_loading.gif') }}"></h2> 
                    </div>
                </div>
            </div>
        </div>		
	</div>
	<hr> 
	<div class="row text-center">
		<button id="botonCargar" class="btn btn-primary" onclick="initProcess();">Cargar datos</button>
		<img id="estadodecarga" class="hidden" style="width: 30px;" src="{{ asset('img/ajax_loading.gif') }}">
	</div>
	<hr>
	<h4>Download Estadisticos Data:</h4>
	<div class="progress">
  		<div id="barraDowload" class="progress-bar" role="progressbar"  aria-valuemin="0" aria-valuemax="100" total-procesos="0"></div>
	</div>
	<h4>Upload Estadisticos Data to the Local Database:</h4>
	<div class="progress">
  		<div id="barraLoad" class="progress-bar bg-success" role="progressbar"  aria-valuemin="0" aria-valuemax="100" total-procesos="0"></div>
	</div>
	<hr>
	<h4>Data Descargada:</h4>
	

	<input type="hidden" name="_token" value="{!! csrf_token() !!}" id="token">
	
	<div >
		<div class="row ct">
			
		</div>
	</div>
@endsection

@section('scripts-vue')
	<script>
		$( document ).ready(function() {

    		BarraProcesosDescarga();
			BarraProcesosCarga();
			//initProcess();

		});
		// Inicializar la barra de procesos
		

		function initProcess()
		{

				var token = $('#token').val();
				let request = $.ajax({
		            headers: {'X-CSRF-Token':token},
		            url:"{{ url('/GetAllCompany') }}",
		            //data: {cantidad},
		            type:'GET',
		            //dataType:'html',
		        });

				// Si la respuesta es exitosa
		        request.done(function( data ) {
						//console.log(data);
		        		let totalCompanies = procesarAllTiker(data);
					  	
				});

		        // Si falla la respuesta
		        request.fail(function( jqXHR, textStatus ) {
					 
				});

				// activar estado de carga
				$('#estadodecarga').removeClass('hidden');

				// bloquear boton get tinker
				$('#botonCargar').attr('disabled', 'true');	 
		}

		//procesar todos los tiker de BD
		function procesarAllTiker(tikers)
		{
			
			let porcentajeProceso = 1 * 100 / (tikers.length);

			tikers.forEach(function(val) 
			{
			  // Consultamos la informacion de cada tiker
			  console.log('consultando la info de empresa '+val.tiker);
			  getDataEstadisticos(val.tiker,porcentajeProceso);

			});

		}

		//cunsultamos la informacion de cada tiker
		function getDataEstadisticos(tiker,porcentajeProceso){

			$dataFinancieros= new Promise((resolve, reject)=>{
					var token = $('#token').val();
					let request = $.ajax({
			            headers: {'X-CSRF-Token':token},
			            url:"{{ url('/GetDataEstadisticos') }}",
			            data: {tiker},
			            type:'GET',
			            dataType:'html',

			        });

					// Si la respuesta es exitosa
			        request.done(function( data ) {
			        		//console.log(data);
							resolve(data);
						  	
					});

			        // Si falla la respuesta
			        request.fail(function( jqXHR, textStatus ) {
						  reject('fallo la descara de la info tiker');
					});	 
				});

			// Process Firt Promises
				Promise.all([$dataFinancieros])
				.then(data => { 
					
					//cargamos la vista con la data
						vistaDataEstadiscos(data,tiker,porcentajeProceso);
						
				})
				.catch(reason => { 

					console.log(reason);
				
				});
			// End Firt promises
			
		}
var cadHTML=[];
		//extraemos los datos financieros de la vista
		function vistaDataEstadiscos(data,tiker,porcentajeProceso)
		{
			// Limpiar DOM
				$('.html').text("");
			// Reasignar datos al DOM
				$('.html').append(data);


			//Parses of HTML string
		 cadHTML = $.parseHTML( $('.html').text() );
			
		 for (var i = 0; i < cadHTML.length; i++) {
		 	if(cadHTML[i].attributes.id=="undefined"){
		 		
		 	}else {
		 		console.log(i);
		 	}
		 }
			
		// detectar posicion donde se encuentran los datos del API
		/*cadHTML.find(function(data, index) 
			{ 
				console.log(data);
				if (data.attributes.id) {

					
						console.log(index);
						var indice=index; 
					
					
				}
				
			});*/

		//console.log('Verificando error'+cadHTML);
		var appHTML = $.parseHTML( cadHTML[indice].innerHTML );
		//console.log(appHTML);
		var tableHTML = $('table', appHTML);

		var groups = [];
		var rows = [];
		var childrenRows = [];
		var k=0;
		var j=0;
		var i=0;
		tableHTML.each( function(){
			var element = $('tr', this);
			j=0;
			rows=[];
			element.each( function(){
				var children = $('td', this);
				k=0;
				childrenRows = [];
				children.each(function(){
					childrenRows[k] = this.innerText;
					k=k+1;
				});

				rows[j] = childrenRows;
				j=j+1;
			});

			groups[i] = rows;
			i=i+1;
		});

		for (var i = 0; i < groups.length; i++) {
			htmlRow = "";
			for (var j = 0; j < groups[i].length; j++) {
				htmlChildren = "";
				for (var k = 0; k < groups[i][j].length; k++) {
					cad = "";
					for (var l = 0; l < groups[i][j][k].length; l++) {
						cad = '<td>'+groups[i][j][k]+'</td>';
					}
					htmlChildren = htmlChildren+cad;
				}
				htmlRow = htmlRow+'<tr>'+htmlChildren+'</tr>';
			}
			
			$('.ct').append('<div class="col-md-12"><table class="table table-condensed " border="1"><tr><td bgcolor="#286090" class="text-center" colspan="2">'+tiker+'</td></tr>'+htmlRow+'</table></div>');
		}

			//console.log(htmlChildren);
			BarraProcesosDescarga(porcentajeProceso);
			saveDataEstadisticos(groups,tiker,porcentajeProceso);
		}

		function saveDataEstadisticos(data,tiker,porcentajeProceso){
			
			$saveDataEstadisticos= new Promise((resolve, reject)=>{
					var token = $('#token').val();
					let request = $.ajax({
			            headers: {'X-CSRF-Token':token},
			            url:"{{ url('/SaveDataEstadisticos') }}",
			            data: {tiker,data},
			            type:'POST',
			            //dataType:'html',

			        });

					// Si la respuesta es exitosa
			        request.done(function( data ) {

							resolve(data);
						  	
					});

			        // Si falla la respuesta
			        request.fail(function( jqXHR, textStatus ) {
						  reject('fallo save info companies ');
					});	 
				});

			// Process Firt Promises
				Promise.all([$saveDataEstadisticos])
				.then(data => { 
				
					console.log('Carga exitosa para '+ tiker);
					BarraProcesosCarga(porcentajeProceso);
					
				})
				.catch(reason => { 

					console.log(reason);
				
				});
			// End Firt promises

		}

		function BarraProcesosDescarga(width="")
		{

			let barra = $('#barraDowload');

			if (width=="") {

				let valor=0;
				barra.attr('style', 'width:'+valor+'%;').text(valor+'%').attr('aria-valuenow', valor);
			}else {
				
				let valorAcumulado =barra.attr('aria-valuenow');
				let valor=parseFloat(Number(valorAcumulado) + Number(width)).toFixed(2);
			
				barra.attr('style', 'width:'+valor+'%;').text(valor+'%').attr('aria-valuenow', valor);
			}	
			
			if (barra.attr('aria-valuenow')>=100 || barra.attr('aria-valuenow')==99  ) {
				valor=100;
				barra.attr('style', 'width:'+valor+'%;').text(valor+'%').attr('aria-valuenow', valor);
				barra.css("background-color", "#32c28a");
	
			}
			
		}

		function BarraProcesosCarga(width="")
		{

			let barra = $('#barraLoad');

			if (width=="") {

				let valor=0;
				barra.attr('style', 'width:'+valor+'%;').text(valor+'%').attr('aria-valuenow', valor);
			}else {
				
				let valorAcumulado = barra.attr('aria-valuenow');
				let valor=parseFloat(Number(valorAcumulado) + Number(width)).toFixed(2);
			
				barra.attr('style', 'width:'+valor+'%;').text(valor+'%').attr('aria-valuenow', valor);
			}	
			
			if (barra.attr('aria-valuenow')>=100 || barra.attr('aria-valuenow')==99  ) {
				valor=100;
				barra.attr('style', 'width:'+valor+'%;').text(valor+'%').attr('aria-valuenow', valor);
				barra.css("background-color", "#32c28a");

				// desactivar estado de carga
				$('#estadodecarga').addClass('hidden');

				// bloquear boton get tinker
				$('#botonCargar').removeAttr('disabled');
				
				// activar painacion del datatable	
				$('#table').DataTable({
					"pageLength": 4, 
				});
			
			}
			
		}

	
	</script>

	
	
@endsection