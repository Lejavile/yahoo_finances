@extends('base')

@section('title')
Companies
@endsection

@section('container')
	
	<div class="html-content" style="display: none;"></div>
	<div class="row">
		<div class="col-md-12">	
			<div class="text-center">
				<small><b>Get Tiker on <a href="https://finviz.com/">https://finviz.com/</a></b></small>
			</div>
		</div>
		<div class="card col-md-6">
            <div class="card-body text-center">
                <h5 class="card-title">Companies o Tiker: Local</h5>
                <div class="d-flex no-block">
                	<div class="ml-auto">
                        <div id="prediction">Total</div>
                    </div>
                    <div class="align-self-end no-shrink">
                        <h2 class="m-b-0" id="idtotalLocal">{{ $totalCompanies or '0' }}</h2>
                        <input type="hidden" name="tikerFinal" value="{{ $tikerFinal or '0' }}"> 
                    </div>
                </div>
            </div>
        </div>
        <div class="card col-md-6">
            <div class="card-body text-center">
                <h5 class="card-title">Companies o Tiker: Finviz</h5>
                <div class="d-flex no-block">
                	<div class="ml-auto">
                        <div id="prediction">Total</div>
                    </div>
                    <div class="align-self-end no-shrink">
                        <h2 class="m-b-0" id="idtotalOnline"><img class="image" style="width: 40px;" src="{{ asset('img/ajax_loading.gif') }}"></h2> 
                    </div>
                </div>
            </div>
        </div>	
	</div>
	<hr>
	<div class="row text-center">
		<button id="botonObtener" class="btn btn-primary" disabled onclick="getListasCompanies()">Get Tiker</button>
		<img id="estadodecarga" class="hidden" style="width: 30px;" src="{{ asset('img/ajax_loading.gif') }}">
	</div>
	<hr>
	<h4>Download Tiker:</h4>
	<div class="progress">
  		<div id="barraDowload" class="progress-bar" role="progressbar"  aria-valuemin="0" aria-valuemax="100" total-procesos="0"></div>
	</div>
	<h4>Load Tiker Database Local:</h4>
	<div class="progress">
  		<div id="barraLoad" class="progress-bar bg-success" role="progressbar"  aria-valuemin="0" aria-valuemax="100" total-procesos="0"></div>
	</div>
	<hr>
	<h3>Downloaded data:</h3>
	<table class="table table-condensed table-hover table-striped table-bordered" id="tikers">
		<thead>
			<tr>
				<th class="text-center">N°</th>
				<th class="text-center">TIKER/SYMBOL</th>
				<th class="text-center">NAME COMPANY</th>
				<th class="text-center">COUNTRY</th>
				<th class="text-center">INDUSTRY</th>
			</tr>
		</thead>
		<tbody class="tbod">
			
		</tbody>
	</table>

	<input type="hidden" name="_token" value="{!! csrf_token() !!}" id="token">
	
@endsection

@section('scripts-vue')
	<script>
		$( document ).ready(function() {

    		BarraProcesosDescarga();
			BarraProcesosCarga();
			initSearchTotalCompaniesOnline();

		});
		// Inicializar la barra de procesos
		

		function initSearchTotalCompaniesOnline(){

			// Firt promises:  Consultar total Empresas Online
				$companiesOnline= new Promise((resolve, reject)=>{
					var token = $('#token').val();
					let request = $.ajax({
			            headers: {'X-CSRF-Token':token},
			            url:"{{ url('/companies/1') }}",
			            //data: {cantidad},
			            type:'GET',
			            dataType:'html',
			        });

					// Si la respuesta es exitosa
			        request.done(function( data ) {

						let totalCompanies = getCompaniesTotal(data);

							resolve(totalCompanies);
						  	
					});

			        // Si falla la respuesta
			        request.fail(function( jqXHR, textStatus ) {
						  reject('fallo companies online');
					});	 
				});

			// Process Firt Promises
				Promise.all([$companiesOnline])
				.then(total => { 

					// print result
					$('#idtotalOnline').text(total);
					$('#botonObtener').removeAttr('disabled');

				})
				.catch(reason => { 

					console.log(reason);
				
				});
			// End Firt promises
		}

		// Obtener total companies online 
		function getCompaniesTotal(data)
		{
			// Limpiar DOM
				$('.html-content').text("");

			// Reasignar datos al DOM
				$('.html-content').append(data);

			// Procesamos el DOM Obtenido
			let cadHTML = $.parseHTML( $('.html-content').text() );
			let secondCadHTML = $.parseHTML(cadHTML[47].innerHTML);
			let threeCadHTML = $.parseHTML(secondCadHTML[1].innerHTML);
			
			// obtenemos el total de compañias online
			totalCompanies = threeCadHTML[0].children[2].children[0].innerText.substr(3,13).replace(/[^\d]/g, '');

			// Retornar datos procesados
			return totalCompanies;
		}

		function getListasCompanies(){

			let variables=requestListInit();
			let paginaInicial=variables.paginainicial;
			let cantidadPaginas=variables.paginafinal;
			//let paginaInicial=0; prueba
			//let cantidadPaginas=3; prueba
			let totalRegistros=20;
			let buscarListas=[];
			let porcentajeProceso= 1 * 100 / (cantidadPaginas-paginaInicial+1);

			
			// Recorremos todas las paginas 
			for (let i = paginaInicial; i <= cantidadPaginas; i++) 
			{
					
				// Creamos una Promises por cada consulta 
				buscarListas['lista'+i]= new Promise((resolve, reject)=>{

					// Buscamos los datos de la lista  
						//variables
						let token = $('#token').val();
						let cantidad = i * totalRegistros + 1; // valor inicial de la busqueda

						// Peticion 
						var request = $.ajax({
				            headers: {'X-CSRF-Token':token},
				            url:"{{ url('/companies') }}/"+ cantidad,
				            //data: {cantidad},
				            type:'GET',
				            dataType:'html',
				        });
						// Si la respuesta es exitosa
				        request.done(function( data ) {

							let listaProcesada = procesarListas(data,cantidad);
							
							resolve(listaProcesada);
							  	
						});

				        // Si falla la respuesta
				        request.fail(function( jqXHR, textStatus ) {
							  reject('fallo'+i);
						});	 
				});					
			}
			//End promises

			// Procesamos las promises creadas por cada lista
			for(var key in buscarListas) 
			{
			  	console.info('Procesando '+key);

			  		Promise.all([buscarListas[key]])
					.then(listaProcesada => { 
		
						let companiasRegistradas = guardarCompanies(listaProcesada[0],porcentajeProceso);
						
							BarraProcesosDescarga(porcentajeProceso);	
						
					})
					.catch(reason => { 

						console.log(reason);
					
					});
			}
			//end proces promises
			
			// activar estado de carga
			$('#estadodecarga').removeClass('hidden');

			// bloquear boton get tinker
			$('#botonObtener').attr('disabled', 'true');
			
			
		}

		function procesarListas(data,cantidad)
		{

			// Limpiar DOM
				$('.html-content').text("");

			// Reasignar datos al DOM
				$('.html-content').append(data);
				
        	// Procesamos el DOM Obtenido
    			let arreglo = [];
				let arregloTodo = [];														
				let cadHTML = $.parseHTML( $('.html-content').text() );
        		let secondCadHTML = $.parseHTML(cadHTML[47].innerHTML);
				let threeCadHTML = $.parseHTML(secondCadHTML[1].innerHTML);
				let fourCadHTML = $.parseHTML(threeCadHTML[0].innerHTML);
				let fiveCadHTML = $.parseHTML(fourCadHTML[6].innerHTML);
				let sixCadHTML = $.parseHTML(fiveCadHTML[1].innerHTML);
				let sevenCadHTML = $.parseHTML(sixCadHTML[1].innerHTML);
				let eightCadHTML = $.parseHTML(sevenCadHTML[1].innerHTML);

        	// Extraemos los datos del DOM
        		for (let i = 0; i < eightCadHTML.length; i++) 
				{
					let tiker = $.parseHTML(eightCadHTML[i].innerHTML);
					arreglo[i] = [tiker[2].innerText, tiker[3].innerText, tiker[6].innerText, tiker[5].innerText, tiker[1].innerText, tiker[4].innerText];
				}
				arreglo[21]=cantidad;
				arregloTodo.push(arreglo);
				
        	// Agregamos los datos al final de la lista	
	        	let cont = 0;
					
				for (let i = 0; i < arregloTodo.length; i++) {
					for (let j = 0; j <21; j++) {
						if(j!=0 && arregloTodo[i][j]){
							cont = cont + 1;
							$('.tbod').append('<tr><td>'+arregloTodo[i][j][4]+'</td><td><a href="/empresa/'+arregloTodo[i][j][0]+'/resumen">'+arregloTodo[i][j][0]+'</a></td><td><a href="/empresa/'+arregloTodo[i][j][0]+'/resumen">'+arregloTodo[i][j][1]+'</a></td><td>'+arregloTodo[i][j][2]+'</td><td>'+arregloTodo[i][j][3]+'</td></tr>');
						}
						
					}
				}

			// Retornar datos procesados
			
        	return arregloTodo;
				
		}

		function guardarCompanies(listaProcesada,porcentajeProceso)
		{

			var token = $('#token').val();
	
			$.ajax({
	            headers: {'X-CSRF-Token':token},
	            url:"{{ url('/ajax/save/companies') }}",
	            data: {arreglo:listaProcesada},
	            type:'POST',
	            dataType:'json',
	            success: function(data){
	            	//Informe por consola
	            	console.info('Lista desde: '+data.lista+ ' procesada!');
	            	console.info('tiker nuevos: '+ data.exito.length+ ' existentes:'+ data.error.length);
	            		// Llenar barra de carga
	            		BarraProcesosCarga(porcentajeProceso);
	            			// Actualizar total de tiker local
	            			$('#idtotalLocal').text(data.totalTiker);
	            			$("input[name='tikerFinal']").val(data.totalTiker);

	            }
	        });

	        
		}

		function BarraProcesosDescarga(width="")
		{

			let barra = $('#barraDowload');

			if (width=="") {

				let valor=0;
				barra.attr('style', 'width:'+valor+'%;').text(valor+'%').attr('aria-valuenow', valor);
			}else {
				
				let valorAcumulado =barra.attr('aria-valuenow');
				let valor=parseFloat(Number(valorAcumulado) + Number(width)).toFixed(2);
			
				barra.attr('style', 'width:'+valor+'%;').text(valor+'%').attr('aria-valuenow', valor);
			}	
			
			if (barra.attr('aria-valuenow')>=100 || barra.attr('aria-valuenow')==99  ) {
				valor=100;
				barra.attr('style', 'width:'+valor+'%;').text(valor+'%').attr('aria-valuenow', valor);
				barra.css("background-color", "#32c28a");
			}
			
		}
		function BarraProcesosCarga(width="")
		{

			let barra = $('#barraLoad');

			if (width=="") {

				let valor=0;
				barra.attr('style', 'width:'+valor+'%;').text(valor+'%').attr('aria-valuenow', valor);
			}else {
				
				let valorAcumulado = barra.attr('aria-valuenow');
				let valor=parseFloat(Number(valorAcumulado) + Number(width)).toFixed(2);
			
				barra.attr('style', 'width:'+valor+'%;').text(valor+'%').attr('aria-valuenow', valor);
			}	
			
			if (barra.attr('aria-valuenow')>=100 || barra.attr('aria-valuenow')==99  ) {
				valor=100;
				barra.attr('style', 'width:'+valor+'%;').text(valor+'%').attr('aria-valuenow', valor);
				barra.css("background-color", "#32c28a");

				// desactivar estado de carga
				$('#estadodecarga').addClass('hidden');

				// bloquear boton get tinker
				$('#botonObtener').removeAttr('disabled');
				
				// activar painacion del datatable	
				$('#tikers').DataTable({
					"pageLength": 20, 
				});
			
			}
			
		}
		// funcion para resolver a partir de que lista iniciar la busqueda en finviz
		function requestListInit(valor="")
		{
			let variables={};
			let nroUltimoTiker = $("input[name='tikerFinal']").val();
			let totalTikerOnline=$('#idtotalOnline').text();
			let tikerPorPagina=20;
			let nroPaginasTotal=parseInt(totalTikerOnline / tikerPorPagina);
			
			if(nroUltimoTiker==0){
				var redondeo=0; 
			}else{
				var redondeo=0.25;
			} 
					
			let tikerFaltantes = totalTikerOnline - nroUltimoTiker;
			let painasFaltantes = Math.round(tikerFaltantes / tikerPorPagina + redondeo);
			let paginaInicial= parseInt(nroPaginasTotal - painasFaltantes);

			variables.paginainicial= paginaInicial;
			variables.paginafinal=nroPaginasTotal;

			return variables;
		}
	
	</script>

	
	
@endsection