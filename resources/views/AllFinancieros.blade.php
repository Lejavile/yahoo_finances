@extends('base')

@section('title')
Companies
@endsection

@section('container')
	
	<div class="html" style="display:none;color: red;"></div>
	<div class="row">
		<div class="col-md-12 text-center ">	
			<h3>DATOS FINANCIEROS</h3>
		</div>
	</div>
	<div class="row py-5">
		<div class="card col-md-4">
            <div class="card-body text-center">
                <h5 class="card-title">Data Cargada</h5>
                <div class="d-flex no-block">
                	<div class="ml-auto">
                        <div id="prediction"></div>
                    </div>
                    <div class="align-self-end no-shrink">
                        <h2 class="m-b-0 " id="idtotalCargados">----<img class="image hidden" style="width: 40px;" src="{{ asset('img/ajax_loading.gif') }}"></h2> 
                    </div>
                </div>
            </div>
        </div>
        <div class="card col-md-4">
            <div class="card-body text-center">
                <h5 class="card-title">Data Pendiente por Cargar</h5>
                <div class="d-flex no-block">
                	<div class="ml-auto">
                        <div id="prediction"></div>
                    </div>
                    <div class="align-self-end no-shrink">
                        <h2 class="m-b-0 " id="idtotalCarga">----<img class="image hidden" style="width: 40px;" src="{{ asset('img/ajax_loading.gif') }}"></h2> 
                    </div>
                </div>
            </div>
        </div>
        <div class="card col-md-4">
            <div class="card-body text-center">
                <h5 class="card-title">Fecha Ultima Carga</h5>
                <div class="d-flex no-block">
                	<div class="ml-auto">
                        <div id=""></div>
                    </div>
                    <div class="align-self-end no-shrink">
                        <h2 class="m-b-0" id="idFecha">----<img class="image hidden" style="width: 40px;" src="{{ asset('img/ajax_loading.gif') }}"></h2> 
                    </div>
                </div>
            </div>
        </div>		
	</div>
	<hr> 
	<div class="row text-center">
		<button id="botonCargar" class="btn btn-primary" onclick="initProcess();">Cargar datos</button>
		<img id="estadodecarga" class="hidden" style="width: 30px;" src="{{ asset('img/ajax_loading.gif') }}">
	</div>
	<hr>
	<h4>Download Financial Data:</h4>
	<div class="progress">
  		<div id="barraDowload" class="progress-bar" role="progressbar"  aria-valuemin="0" aria-valuemax="100" total-procesos="0"></div>
	</div>
	<h4>Upload Financial Data to the Local Database:</h4>
	<div class="progress">
  		<div id="barraLoad" class="progress-bar bg-success" role="progressbar"  aria-valuemin="0" aria-valuemax="100" total-procesos="0"></div>
	</div>
	<hr>
	<h4>Data Descargada:</h4>
	

	<input type="hidden" name="_token" value="{!! csrf_token() !!}" id="token">
	
	<table id="table" class="table table-striped" border="1">
		
		
	</table>
@endsection

@section('scripts-vue')
	<script>
		$( document ).ready(function() {

    		BarraProcesosDescarga();
			BarraProcesosCarga();
			//initProcess();

		});
		// Inicializar la barra de procesos
		

		function initProcess()
		{

				var token = $('#token').val();
				let request = $.ajax({
		            headers: {'X-CSRF-Token':token},
		            url:"{{ url('/GetAllCompany') }}",
		            //data: {cantidad},
		            type:'GET',
		            //dataType:'html',
		        });

				// Si la respuesta es exitosa
		        request.done(function( data ) {
					
		        		let totalCompanies = procesarAllTiker(data);
					  	
				});

		        // Si falla la respuesta
		        request.fail(function( jqXHR, textStatus ) {
					 
				});

				// activar estado de carga
				$('#estadodecarga').removeClass('hidden');

				// bloquear boton get tinker
				$('#botonCargar').attr('disabled', 'true');	 
		}

		//procesar todos los tiker de BD
		function procesarAllTiker(tikers)
		{
			
			let porcentajeProceso = 1 * 100 / (tikers.length);

			tikers.forEach(function(val) 
			{
			  // Consultamos la informacion de cada tiker
			  console.log('consultando la info de empresa '+val.tiker);
			  getDataFinancial(val.tiker,porcentajeProceso);

			});

		}

		//cunsultamos la informacion de cada tiker
		function getDataFinancial(tiker,porcentajeProceso){

			$dataFinancieros= new Promise((resolve, reject)=>{
					var token = $('#token').val();
					let request = $.ajax({
			            headers: {'X-CSRF-Token':token},
			            url:"{{ url('/GetDataFinancieros') }}",
			            data: {tiker},
			            type:'GET',
			            dataType:'html',

			        });

					// Si la respuesta es exitosa
			        request.done(function( data ) {

							resolve(data);
						  	
					});

			        // Si falla la respuesta
			        request.fail(function( jqXHR, textStatus ) {
						  reject('fallo la descara de la info tiker');
					});	 
				});

			// Process Firt Promises
				Promise.all([$dataFinancieros])
				.then(data => { 
				
					//cargamos la vista con la data
						vistaDataFinancieros(data,tiker,porcentajeProceso);
						
				})
				.catch(reason => { 

					console.log(reason);
				
				});
			// End Firt promises
			
		}

		//extraemos los datos financieros de la vista
		function vistaDataFinancieros(data,tiker,porcentajeProceso)
		{
			// Limpiar DOM
				$('.html').text("");
			// Reasignar datos al DOM
				$('.html').append(data);


			//Parses of HTML string
			var cadHTML = $.parseHTML( $('.html').text() );
			
			var appHTML = $.parseHTML( cadHTML['59'].innerHTML );
			var tableHTML = $('table', appHTML);
			var rows = tableHTML.children().children().children();
			
			//array with dates
			var dates = [rows[1].innerText, rows[2].innerText, rows[3].innerText, rows[4].innerText ];

			//array with titles of financial data
			var titles = [ "Revenue", "Operating Expenses", "Income from Continuing Operations", "Non-recurring Events", "Net Income" ];

	 		//array for help
			var array = [];
			var arrayResult = [];
			
			//put data text in array for help
			for (var i = 0; i < rows.length; i++) {
				array[i] = rows[i].innerText;
			}

			//extract array clear titles and dates

			var bandera = 0;
			for (var i = 0; i < array.length; i++) {
				for (var j = 0; j < titles.length; j++) {
					if(titles[j]==array[i]){
						if(array[i]=="Net Income"&&bandera==0){
							bandera=1;
							array[i]="*";
						}

						if(bandera==0){
							array[i]="*";
						}
						
					}
				}
				for (var k = 0; k < dates.length; k++) {
					if(dates[k]==array[i]){
						array[i] = "*";
					}
				}
				
			}

			// for (var i = 0; i < titles.length; i++) {
			// 	arrayResult[i] = { title:titles[i], dates: [] };
			// 	for (var j = 0; j < dates.length; j++) {
			// 		arrayResult[i].dates[j] = {date: dates[j]}
			// 	}
				 
			// }

			var array2 = [];
			var array3 = [];
			var m = 0;

			for (var i = 0; i < array.length; i++) {

				if(array[i]=="*"){
					
				}else{
					array2[m] = array[i];
					m=m+1;
				}
				
				if(m==5){
					array3.push(array2);
					m=0;
					array2=[];
				}

				
			}


			var arreglo1 = [];
			
			$('#table').append('<tr><td bgcolor="#286090" class="text-center" colspan="5"><h4 style="color:white;">'+ tiker +'</h4></td></tr>');
			
			for (var i = 0; i < array3.length; i++) {
					
					$('#table').append('<tr><td><b>'+array3[i][0]+'</b></td><td>'+array3[i][1]+'</td><td>'+array3[i][2]+'</td><td>'+array3[i][3]+'</td><td>'+array3[i][4]+'</td></tr>');
				
			}

			//console.log(array3);
			BarraProcesosDescarga(porcentajeProceso);
			saveDataFinanciero(array3,tiker,porcentajeProceso);
		}

		function saveDataFinanciero(data,tiker,porcentajeProceso){

			$saveDataFinancieros= new Promise((resolve, reject)=>{
					var token = $('#token').val();
					let request = $.ajax({
			            headers: {'X-CSRF-Token':token},
			            url:"{{ url('/SaveDataFinancieros') }}",
			            data: {tiker,data},
			            type:'POST',
			            //dataType:'html',

			        });

					// Si la respuesta es exitosa
			        request.done(function( data ) {

							resolve(data);
						  	
					});

			        // Si falla la respuesta
			        request.fail(function( jqXHR, textStatus ) {
						  reject('fallo save info companies ');
					});	 
				});

			// Process Firt Promises
				Promise.all([$saveDataFinancieros])
				.then(data => { 
				
					console.log('Carga exitosa para '+ tiker);
					BarraProcesosCarga(porcentajeProceso);
					
				})
				.catch(reason => { 

					console.log(reason);
				
				});
			// End Firt promises

		}

		function BarraProcesosDescarga(width="")
		{

			let barra = $('#barraDowload');

			if (width=="") {

				let valor=0;
				barra.attr('style', 'width:'+valor+'%;').text(valor+'%').attr('aria-valuenow', valor);
			}else {
				
				let valorAcumulado =barra.attr('aria-valuenow');
				let valor=parseFloat(Number(valorAcumulado) + Number(width)).toFixed(2);
			
				barra.attr('style', 'width:'+valor+'%;').text(valor+'%').attr('aria-valuenow', valor);
			}	
			
			if (barra.attr('aria-valuenow')>=100 || barra.attr('aria-valuenow')==99  ) {
				valor=100;
				barra.attr('style', 'width:'+valor+'%;').text(valor+'%').attr('aria-valuenow', valor);
				barra.css("background-color", "#32c28a");
	
			}
			
		}

		function BarraProcesosCarga(width="")
		{

			let barra = $('#barraLoad');

			if (width=="") {

				let valor=0;
				barra.attr('style', 'width:'+valor+'%;').text(valor+'%').attr('aria-valuenow', valor);
			}else {
				
				let valorAcumulado = barra.attr('aria-valuenow');
				let valor=parseFloat(Number(valorAcumulado) + Number(width)).toFixed(2);
			
				barra.attr('style', 'width:'+valor+'%;').text(valor+'%').attr('aria-valuenow', valor);
			}	
			
			if (barra.attr('aria-valuenow')>=100 || barra.attr('aria-valuenow')==99  ) {
				valor=100;
				barra.attr('style', 'width:'+valor+'%;').text(valor+'%').attr('aria-valuenow', valor);
				barra.css("background-color", "#32c28a");

				// desactivar estado de carga
				$('#estadodecarga').addClass('hidden');

				// bloquear boton get tinker
				$('#botonCargar').removeAttr('disabled');
				
				// activar painacion del datatable	
				/*$('#table').DataTable({
					"pageLength": 4, 
				});*/
			
			}
			
		}

	
	</script>

	
	
@endsection