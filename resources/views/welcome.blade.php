<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">


</head>
<body>
	<header>
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<h2>adidas AG (ADS.DE)</h2>
				</div>

				<div class="col-md-6" style="margin-top: 1.5em">
					<input type="text" class="form-control" placeholder="Search">
				</div>
			</div>
			
		</div>
	</header>
    

	<div class="container">
		<h3>History</h3>
		<br>
		<table class="table table-hover table-condensed" id="history">
	    	<thead>
	    		<tr>
	    			<th>Date</th>
	    			<th>Open</th>
	    			<th>High</th>
	    			<th>Low</th>
	    			<th>Close</th>
	    			<th>AdjClose</th>
	    			<th>Volume</th>
	    		</tr>
	    	</thead>
	    	<tbody>
	    		@foreach($arrayFormatHistory as $item)
	    		<tr>
	    			<td>{{ $item['date']['date'] }}</td>
	    			<td>{{ $item['open'] }}</td>
	    			<td>{{ $item['high'] }}</td>
	    			<td>{{ $item['low'] }}</td>
	    			<td>{{ $item['close'] }}</td>
	    			<td>{{ $item['adjClose'] }}</td>
	    			<td>{{ $item['volume'] }}</td>
	    		</tr>
	    		@endforeach
	    	</tbody>
	    </table>
		<br>
		<div class="text-center">
			<a href="{{ url('generar/excel') }}" class="btn btn-primary">Generar Excel</a>
		</div>
	    
	</div>
    
</body>
</html>
<script src="{{ asset('js/jquery.js') }}"></script>
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>

    <script>
	$('#history').DataTable({
		"order": [[ 1, "desc" ]],
	});
</script>



