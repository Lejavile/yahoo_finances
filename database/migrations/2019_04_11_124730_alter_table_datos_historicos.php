<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableDatosHistoricos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('datos_historicos', function (Blueprint $table) {

        $table->string('pms55')->nullable()->after('close');
        $table->string('pms200')->nullable()->after('pms55');
        

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('datos_historicos', function (Blueprint $table) {
            //
            $table->dropColumn('pms55');
            $table->dropColumn('pms200');
            
        });
    }
}
