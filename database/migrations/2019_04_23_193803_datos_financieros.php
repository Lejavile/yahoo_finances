<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DatosFinancieros extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('datos_financieros', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tiker', 45); 
            $table->string('titulo', 45)->nullable();
            $table->string('valor1', 45)->nullable();
            $table->string('valor2', 45)->nullable();
            $table->string('valor3', 45)->nullable();
            $table->string('valor4', 45)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
