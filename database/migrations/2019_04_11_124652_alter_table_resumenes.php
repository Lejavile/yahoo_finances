<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableResumenes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('resumenes', function (Blueprint $table) {

        $table->date('fresumen')->nullable()->after('prevision');
        $table->date('rango52Semanas_minimo')->nullable()->after('rango52Semanas');
        $table->date('rangoDiario_minimo')->nullable()->after('rangoDiario');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('resumenes', function (Blueprint $table) {
            //
            $table->dropColumn('fresumen');
            $table->dropColumn('rango52Semanas_minimo');
            $table->dropColumn('rangoDiario_minimo');
            
            
        });
    }
}
